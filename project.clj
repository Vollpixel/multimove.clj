(defproject multimove "2.2.7"
  :description "Mutlimove renames image files in a directory"
  :url "https://gitlab.com/Experience/multimove.clj"
  :license {:name "GNU General Public License Version 3"
            :url "http://www.gnu.org/licenses/gpl-3.0.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/tools.cli "0.4.2"]
                 [clojure-ini "0.0.2"]]
  :main ^:skip-aot multimove.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
