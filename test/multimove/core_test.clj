(ns multimove.core-test
  (:require [clojure.test :refer :all]
            [multimove.core :refer :all]
            [clojure.string :as str]
            [clojure.java.io :as io]))

(defn test-fixture [f]
  (def test-settings
    {:do-rename false
     :camera "D300"
     :from-pattern "DSC_*.*"
     :to-pattern "$t-$1-$d-$c.$2"
     :new-fname-elems (to-pattern-elems "$t-$1-$d-$c.$2")
     :time-shift 0})
  (f))

(use-fixtures :once test-fixture)

(deftest test-error-msg
  (testing "Make sure that error messages are formatted correctly"
    (let [err-msg (error-msg ["line 1" "line 2"])]
      ;; Must contain 4 lines
      (is (= 4 (count (str/split err-msg #"\n")))))))

(deftest test-correct-file-ext-with-dot
  (testing "Get correct extension without dot"
    (let [ext (correct-file-ext ".jpg")]
      ;; Must not contain '.'
      (is (= "jpg" ext)))))

(deftest test-correct-file-ext-without-dot
  (testing "Get correct extension without dot"
    (let [ext (correct-file-ext "jpg")]
      ;; Must not contain '.'
      (is (= "jpg" ext)))))

(deftest test-regex-from-pattern
  (testing "Get correct pattern"
    (let [pat (regex-from-pattern (:from-pattern test-settings))]
      ;; Must match "DSC_1234.JPG"
      (is (= 3 (count (re-matches pat "DSC_1234.JPG")))))))

(deftest test-to-pattern-elems
  (testing "Get correct list of to-pattern"
    (let [elems (vec (:new-fname-elems test-settings))]
      (is (= 0 (compare elems ["$t" "-" "$1" "-" "$d" "-" "$c" "." "$2"]))))))

(deftest test-new-fname
  (testing "Get new file name: DSC_1234.JPG -> Foto_location-1234-1970.01.01-D300.jpg"
    (let [new-fname (new-file-name (io/file "DSC_1234.JPG")
                                   (regex-from-pattern (:from-pattern test-settings))
                                   (:time-shift test-settings)
                                   (:new-fname-elems test-settings)
                                   (:camera test-settings)
                                   "Foto location")]
      (is (not (= nil (re-matches #"Foto_location-1234-[0-9]+\.[0-9]+\.[0-9]+-D300\.jpg" new-fname)))))))
