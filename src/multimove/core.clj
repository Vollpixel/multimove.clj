(ns multimove.core
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure-ini.core :as ini]
            [clojure.tools.cli :as cli]
            [clojure.pprint :as pprint])
  (:gen-class))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def this-file-name "multimove.clj")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Multimove - a multi-file renaming tool
;;
;; Copyright (C) 2012-2019, Peter Stoldt
;;
;; Licensed under GNU General Public License Version 3
;;
;;     http://www.gnu.org/licenses/gpl-3.0.html
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.
;;
(def this-author "Peter Stoldt")
;;
;;   v1.0        25-Jul-2012   First version in Python 2
;;   v1.1        19-Aug-2012   Minor change
;;   v1.2        07-Nov-2012   Fix time/date problems, add patterns for undo
;;   v1.2.1      25-Nov-2012   Fix that Win7 does not allow ':' in file name
;;   v1.2.2      01-Apr-2013   Handle date and time as separated values
;;   v1.2.3      24-Jul-2013   Improve camera patterns
;;   v1.3.0      10-Aug-2014   Port to Python 3
;;   v2.0.0.clj  10-Mar-2016   Rewritten in Clojure, new features added
;;   v2.1.0      26-Dec-2016   Bugfixes, Toggle yes-no, cleanups
;;   v2.2.0      02-Jan-2017   Move movie files into a directory
;;   v2.2.1      04-Jan-2017   Don't create dirs when no files are moved
;;   v2.2.2      05-Jan-2017   Refactor: Improve testability
;;   v2.2.3      23-Apr-2017   Refactor: Function names, remove atom in new-fname
;;   v2.2.4      13-May-2017   Refactor: Remove side effects from new-fname
;;   v2.2.5      05-Jun-2017   Improved unit-tests; List cameras
;;   v2.2.6      12-Jun-2017   Change Camera afterwards
;;   v2.2.7      05-Dec-2019   Updated component versions
;;
(def this-file-version "v2.2.7")
(def this-last-modified "05-Dec-2019")
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(def this-version (format "%s %s (%s) by %s" this-file-name this-file-version
                          this-last-modified this-author))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Planned Features
;; ----------------
;;
;;  [ ] Get picture date/time from EXIF data, not from file
;;      See https://github.com/olimcc/clj-exif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; For the user interface
;;================================================================

(def -line "----------------------------------------------------------------")
(def =line "================================================================")

(def config-dir (io/file (System/getenv "HOME") ".config" "multimove"))
(def config-file (io/file config-dir "multimove.conf"))
(def resource-config-file (io/resource "multimove.conf"))

(def config (atom {}))
(def settings (atom {:do-rename true}))
(def execute (atom false))

(def cli-options
  [["-d" "--ooc-dir OOC-DIRECTORY" "Directory to where the ooc files will be moved"]
   ["-e" "--ooc-ext OOC-EXT" "File extension of the ooc files"]
   ["-m" "--movie-dir MOVIE-DIR" "Directory to where the movie files will be moved"]
   ["-n" "--movie-ext MOVIE-EXT" "File extension of the movie files"]
   ["-t" "--test TEST-DIR" "Run in test mode and do not change anything"]
   ["-l" "--list" "List defined cameras in configuration"]
   ["-h" "--help"]])

;; Functions for the CLI
;;================================================================

(defn usage
  [options-summary]
  (->> [""
        "Usage:"
        ""
        "  multimove [<camera>] [-d <ooc-dir>] [-e <ooc-file-ext>] [-m <movie-dir>] [-n <movie-file-ext>]"
        "  multimove [-l|--list]"
        ""
        "Options:"
        ""
        "  <camera>   Use <kamera-profile> instead of the default profile defined in the"
        "             config file"
        options-summary
        ""
        "Examples:"
        ""
        "  multimove"
        "  multimove iP5S"
        "  multimove D750 -d OOC -e jpg -m Movies -n mpg"
        "  multimove D750 -d RAW -e nef"
        "  multimove D750.undo"
        ""
        "Description:"
        ""
        "  multimove is a program to rename picture files copied from a camera.\n"]
       (str/join \newline)))

(defn error-msg
  [errors]
  (str "ERROR: The following errors occurred:\n\n"
       (str/join \newline errors)))

(defn exit
  [status msg]
  (println msg)
  (System/exit status))

;; Helper functions
;;================================================================

(defn get-conf
  "Simplify configuration access"
  [name-space elem]
  (elem (name-space @config)))

(defn correct-file-ext
  "Make sure, that the file extension does not contain a '.'"
  [file-ext]
  (str/replace file-ext "." ""))

(defn regex-from-pattern
  "Convert given 'from' pattern to a regular expression and parameter list"
  [raw-from-pattern]
  (let [pat-str (str/replace raw-from-pattern "." "\\.")]
    (re-pattern (str/replace pat-str "*" "([0-9A-Za-z]*)"))))

(defn dir-file-list
  "Walk through a directory and return a list of files, matching the regular expression"
  [dirpath name-regex]
  (doall (filter #(re-matches name-regex (.getName %))
                 (file-seq (io/file dirpath)))))

(defn to-pattern-elems
  "Return the elemens of the to-pattern"
  [to-pattern]
  (re-seq #"\$[cdet0-9]|[\W]" to-pattern))

(defn new-file-name
  "Get the new file name by applying the regex to the original file name
  and put its elements into the new pattern"
  [file from-regex time-shift fname-elems camera title]

  (let [orig-name (.getName file)
        fname-parts (rest (re-find from-regex orig-name))
        file-time (+ (.lastModified file) (* time-shift 3600 1000))
        file-date-str (.format (java.text.SimpleDateFormat. "yyyy.MM.dd") (java.util.Date. file-time))
        file-time-str (.format (java.text.SimpleDateFormat. "HH:mm:ss") (java.util.Date. file-time))]

    ;; new-fname-elems: ("$t" "-" "$1" "-" "$d" "-" "$c" "." "$2")
    (loop [in fname-elems
           out ""]
      (if (empty? in)
        out
        (recur (rest in)
               (let [elem (first in)]
                 (cond
                   ;; camera
                   (= elem "$c") (do (str out camera))
                   ;; date
                   (= elem "$d") (do (str out file-date-str))
                   ;; time
                   (= elem "$e") (do (str out file-time-str))
                   ;; file-title
                   (= elem "$t") (do (str out (str/replace title #"\s" "_")))
                   ;; original file-name parts
                   (re-matches #"\$[1-9]" elem) (do
                                                  (let [fname-part (nth fname-parts
                                                                        (- (Integer. (re-find #"\d+" elem)) 1))]
                                                    (if (= elem (last fname-elems))
                                                      ;; convert file extension to lowercase
                                                      (str out (str/lower-case fname-part))
                                                      (str out fname-part))))
                   :else (str out elem))))))))

(defn rename-files
  "Rename the files and move file to target directory, if desired"
  [test]

  (if (true? test)
    (println "Test-mode: No files renamed")
    (let [from-regex (regex-from-pattern (:from-pattern @settings))
          file-list (dir-file-list (:src-dir @settings) from-regex)
          time-shift (:time-shift @settings)
          fname-elems (:new-fname-elems @settings)
          camera (:camera @settings)
          title (:file-title @settings)]
      (doseq [file file-list]
        (let [new-name (new-file-name file from-regex time-shift fname-elems camera title)]
          (do (println (.getName file) "->" new-name)
              (.renameTo file (io/file new-name))))))))

(defn move-files
  "Move defined files into directories"
  [test target-dir file-ext]

  (if (true? test)
    (println "Test-mode: No files moved")
    (let [file-list (dir-file-list (:src-dir @settings) (re-pattern (str ".*\\." file-ext)))
          to-dir (io/file (:src-dir @settings) target-dir)]
      (if (> (count file-list) 0)
        (do (.mkdir to-dir)
            (doseq [file file-list]
              (let [target-file (io/file to-dir (.getName file))]
                (println (.getName file) "->" (.getAbsolutePath target-file))
                (.renameTo file target-file))))))))

(defn defined-cameras
  "Get a list of defined cameras"
  []
  (let [cam-keys (filter #(not= 0 (compare :defaults %)) (keys @config))]
    (sort (map #(name %) cam-keys))))

(defn list-cameras
  "Print a list of all defined cameras in the configuration"
  []
  (println "\nDefined Cameras:")
  (println -line)
  (doseq [camera (defined-cameras)]
    (println (str " - " camera)))
  (println -line))

;; User interaction
;;================================================================

(defn input-yes-or-no
  "Get yes or no answer from user"
  []
  (let [yesno (read-line)]
    (if (= (first (str/lower-case yesno)) \y)
      true
      false)))

(defn toggle-yes-no
  "Toggle binary settings"
  [setting]
  (not setting))

(defn get-choice
  "Get choice of a menu"
  [prompt]
  (print (str prompt ": "))
  (flush)
  (first (str/lower-case (read-line))))

(defn get-str-setting
  "Get a string value. Uses the given string as input prompt"
  [key prompt]
  (print (str "\n" prompt ": "))
  (flush)
  (swap! settings assoc key (read-line)))

(defn get-int-setting
  "Get an integer value. Uses the given string as input prompt"
  [key prompt]
  (print (str "\n" prompt ": "))
  (flush)
  (swap! settings assoc key (read-string (read-line))))

(defn toggle-bool-setting
  "Toggle a boolean value. Uses the given string as input prompt"
  [key]
  (swap! settings assoc key (toggle-yes-no (key @settings))))

(defn change-camera
  "Change the selected camera"
  []
  (list-cameras)
  (let [old-camera (:camera @settings)
        new-camera (get-str-setting :camera "Camera")]
    (if (> (.indexOf (defined-cameras) (:camera @settings)) -1)
      (do
        (swap! settings assoc :ooc-file-ext (correct-file-ext
                                              (get-conf (keyword (:camera @settings)) :ooc-file-ext)))
        (swap! settings assoc :movie-file-ext (correct-file-ext
                                                (get-conf (keyword (:camera @settings)) :movie-file-ext)))
        (swap! settings assoc :from-pattern (get-conf (keyword (:camera @settings)) :from-pattern))
        (swap! settings assoc :to-pattern (get-conf (keyword (:camera @settings)) :to-pattern))
        (swap! settings assoc :new-fname-elems
               (to-pattern-elems (get-conf (keyword (:camera @settings)) :to-pattern))))
      (do
        (swap! settings assoc :camera old-camera)
        (println "\nThe specified camera is not defined\n")))))

(defn show-preview
  "Show a preview of the planned changes"
  []
  (let [from-regex (regex-from-pattern (:from-pattern @settings))
        file-list (dir-file-list (:src-dir @settings) from-regex)
        time-shift (:time-shift @settings)
        fname-elems (:new-fname-elems @settings)
        camera (:camera @settings)
        title (:file-title @settings)]
    (println =line)
    (doseq [file file-list]
      (let [new-name (new-file-name file from-regex time-shift fname-elems camera title)]
        (println (.getName file) "->" new-name)))))

(defn show-menu
  "Show the menu"
  []
  (println =line)
  (println "   Path   :" (:src-dir @settings))
  (println -line)
  (println "a) Camera                    :" (:camera @settings))
  (println "b) File Title                :" (:file-title @settings))
  (println "c) From-Pattern              :" (:from-pattern @settings)
           "-->" (regex-from-pattern (:from-pattern @settings)))
  (println "d) To-Pattern                :" (:to-pattern @settings))
  (println "e) Time shift in hours (h)   :" (:time-shift @settings))
  (println -line)
  (println "f) OOC target directory      :" (:ooc-target-dir @settings))
  (println "g) OOC file extension        :" (:ooc-file-ext @settings))
  (println "h) Movie target directory    :" (:movie-target-dir @settings))
  (println "i) Movie file extension      :" (:movie-file-ext @settings))
  (println "k) Move OOC files to dir?    :" (if (true? (:do-ooc-file-move @settings)) "yes" "no"))
  (println "l) Move movie files to dir?  :" (if (true? (:do-movie-file-move @settings)) "yes" "no"))
  ;; (println "t) Test mode?                :" (if (true? (:do-rename @settings)) "no" "yes"))
  (println -line)
  (println "p) Show preview")
  (println "x) Execute")
  (println "q) Quit without execution")
  (if (true? (:test @settings))
    (do (println =line)
        (println " == TEST MODE ==")
        (println =line))
    (println -line))
  (flush))

(defn menu
  "Show the menu and change settings"
  []
  (get-str-setting :file-title "Description")

  (show-preview)

  (while (false? @execute)
    (do (show-menu)
        (let [choice (get-choice "Please select")]
          (cond
            (= choice \a) (change-camera)
            (= choice \b) (get-str-setting :file-title "Description")
            (= choice \c) (get-str-setting :from-pattern "From-Pattern")
            (= choice \d) (do (get-str-setting :to-pattern "To-Pattern")
                              (swap! settings assoc :new-fname-elems
                                     (to-pattern-elems (:to-pattern @settings))))
            (= choice \e) (get-int-setting :time-shift "Time shift in hours")
            (= choice \f) (get-str-setting :ooc-target-dir "OOC target Directory")
            (= choice \g) (get-str-setting :ooc-file-ext "OOC file Extension")
            (= choice \h) (get-str-setting :movie-target-dir "Movie target Directory")
            (= choice \i) (get-str-setting :movie-file-ext "Movie file Extension")
            (= choice \k) (toggle-bool-setting :do-ooc-file-move)
            (= choice \l) (toggle-bool-setting :do-movie-file-move)
            ;; (= choice \t) (toggle-bool-setting :do-rename)
            (= choice \p) (show-preview)
            (= choice \x) (reset! execute true)
            (= choice \q) (do (swap! settings assoc :do-rename false)
                              (reset! execute true)))))))

;; Config file handling
;;================================================================

(defn create-config-file
  "Create configuration file from the example file in the resources"
  []
  (do (.mkdirs config-dir)
      (spit config-file
            (slurp resource-config-file))))

(defn read-config
  "Read the config file from file system"
  []
  (if (not (.exists config-file))
    (create-config-file))
  (ini/read-ini config-file :keywordize? true))

(defn init-config
  "Initialize and set-up the configuration for the use in the application"
  [args]

  (let [{:keys [options arguments errors summary]} (cli/parse-opts args cli-options)]

    (reset! config (read-config))

    (if errors
      (exit 1 (error-msg errors)))

    (if (> (count arguments) 1)
      (do
        (println (error-msg (list "Too many arguments")))
        (exit 1 (usage summary))))

    (if (= 1 (count arguments))
      (swap! settings assoc :camera (first arguments))
      (swap! settings assoc :camera (get-conf :defaults :used-camera)))

    (if (:help options)
      (exit 0 (usage summary)))
    (if (:list options)
      (do (list-cameras)
          (exit 0 "")))
    (if (:ooc-dir options)
      (swap! settings assoc :ooc-target-dir (:ooc-dir options))
      (swap! settings assoc :ooc-target-dir (get-conf :defaults :ooc-target-dir)))
    (if (:movie-dir options)
      (swap! settings assoc :movie-target-dir (:movie-dir options))
      (swap! settings assoc :movie-target-dir (get-conf :defaults :movie-target-dir)))
    (if (:ooc-ext options)
      (swap! settings assoc :ooc-file-ext (correct-file-ext (:ooc-ext options)))
      (swap! settings assoc :ooc-file-ext (correct-file-ext
                                            (get-conf (keyword (:camera @settings)) :ooc-file-ext))))
    (if (:movie-ext options)
      (swap! settings assoc :movie-file-ext (correct-file-ext (:ooc-ext options)))
      (swap! settings assoc :movie-file-ext (correct-file-ext
                                              (get-conf (keyword (:camera @settings)) :movie-file-ext))))
    (if (:test options)
      (do (swap! settings assoc :test true)
          (swap! settings assoc :src-dir (.getAbsolutePath (io/file (:test options)))))
      (do (swap! settings assoc :test false)
          (swap! settings assoc :src-dir (.getAbsolutePath (io/file ".")))))

    (swap! settings assoc :file-title "Title not set")
    (swap! settings assoc :from-pattern (get-conf (keyword (:camera @settings)) :from-pattern))
    (swap! settings assoc :to-pattern (get-conf (keyword (:camera @settings)) :to-pattern))
    (swap! settings assoc :new-fname-elems
           (to-pattern-elems (get-conf (keyword (:camera @settings)) :to-pattern)))
    (swap! settings assoc :time-shift 0) ;; in hours
    (swap! settings assoc :do-ooc-file-move
           (if (= (first (str/lower-case (get-conf :defaults :move-ooc-files))) \y) true false))
    (swap! settings assoc :do-movie-file-move
           (if (= (first (str/lower-case (get-conf :defaults :move-movie-files))) \y) true false))
    (swap! settings assoc :do-rename true)))

;; Main
;;================================================================

(defn -main
  "Execute it as command line tool"
  [& args]

  (println (str this-version "\n"))
  (init-config args)
  (menu)

  (if (true? (:do-rename @settings))
    (do (rename-files (:test @settings))
        (if (true? (:do-ooc-file-move @settings))
          (move-files (:test @settings) (:ooc-target-dir @settings) (:ooc-file-ext @settings)))
        (if (true? (:do-movie-file-move @settings))
          (move-files (:test @settings) (:movie-target-dir @settings) (:movie-file-ext @settings))))
    (println "OK, may be later...")))

;; (init-config [])
;; (-main "--test" "test/data")
;; (-main "-d" "ABC" "-e" "mpg")
