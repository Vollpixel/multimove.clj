# Multimove

Multimove - a multi-file renaming tool to rename picture files copied from a camera.

## Installation

Multimove is a leiningen (http://leiningen.org/) project and is implemented in Clojure.
To get a binary (a JAR file), follow these steps:

- Download from this page or clone the repository
- Call `lein uberjar` to build the JAR file
- Check the `target` directory for the JAR file

## Usage

    $ java -jar multimove.jar [args]

### Usage

    multimove [<camera>] [-d <ooc-dir>] [-e <ooc-file-ext>] [-m <movie-dir>] [-n <movie-file-ext>]"

### Options

    <camera>  Use <kamera-profile> instead of the default profile defined in the
              config file
    -d, --ooc-dir OOC-DIRECTORY  Directory to where the ooc files will be moved
    -e, --ooc-ext OOC-EXT        File extension of the ooc files, e.g. 'jpg'
    -m, --movie-dir MOVIE-DIR    Directory to where the movie files will be moved
    -n, --movie-ext MOVIE-EXT    File extension of the movie files, e.g. 'mov'
    -t, --test TEST-DIR          Run in test mode and do not change anything
    -h, --help

### Examples

    multimove
    multimove iP5S
    multimove D750 -d OOC -e jpg -m Movies -n mpg
    multimove D750 -d RAW -e nef
    multimove D750.undo

### Description

multimove is a program to rename picture files copied from a camera.

## License

Copyright © 2012-2019, Peter Stoldt

Licensed under GNU General Public License Version 3

    http://www.gnu.org/licenses/gpl-3.0.html

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.
